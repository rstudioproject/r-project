# installing a package for dealing with big data
library('readr')
#installing a package for slice function for easier computing 
library('dplyr')

# uploading to R the needed data with the new package command, with size limitation for easier computing
sample_submission<-read.csv(file='sample_submission.csv',nrows = 5000000)
clicks_test<-read.csv(file='clicks_test.csv',nrows = 5000000)
clicks_train<-read.csv(file='clicks_train.csv',nrows = 5000000)
documents_topics<-read.csv(file='documents_topics.csv',nrows = 5000000)
documents_categories<-read.csv(file = 'documents_categories.csv',nrows = 5000000)
events<-read.csv(file='events.csv',nrows = 5000000)
page_views_sample<-read.csv(file='page_views_sample.csv',nrows = 5000000)
promoted_content<-read.csv(file='promoted_content.csv',nrows = 5000000)
documents_entities<-read.csv(file = 'documents_entities.csv',nrows = 5000000)
documents_meta<-read.csv(file = 'documents_meta.csv',nrows = 5000000)

#merging for making a flat file
help_table<- merge(events,clicks_train,by=('display_id'),all=TRUE)
help_table2<-merge(help_table,page_views_sample,by=c('uuid',"document_id"), all=TRUE)
help_table3<-na.omit(help_table2)
help_table<-merge(help_table3,documents_categories,by=c('document_id'))
help_table2<-merge(help_table,documents_topics,by=c('document_id'))
help_table3<-merge(help_table2,documents_meta,by=c('document_id'))
help_table<-merge(help_table3,promoted_content,by=c('ad_id'))
final_table<-help_table[,c(-5,-6,-7)]#deleting duplicated columns


#renaming columns for the flat file
colnames(final_table)[2]<-"site_showing_ad"
colnames(final_table)[11]<-"category_confidence_level"
colnames(final_table)[13]<-"topic_confidence_level"
colnames(final_table)[17]<-"destined_site_by_ad"
flat_file<- final_table[,c(colnames(final_table)[colnames(final_table)!='clicked'],'clicked')]


library('plyr')

#ordering flat_file by display_id
sort_table<-arrange(flat_file,display_id)
sort_table<-distinct(sort_table)

#spliting to train and test by 30%-70%
split<-runif(nrow(sort_table))>0.3

data_train1<-sort_table[split,]
data_test1<-sort_table[!split,]


display_id_test<-data_test1[,c(4)]
data_train<-data_train1[,c(-4)]
data_test<-data_test1[,c(-4)]

#base naive algorithem 
library('e1071')
library('SDMTools')
conv_click<-function(x){x<-ifelse(x=='1',1,0)}
data_train$clicked<-as.factor(data_train$clicked)
data_test$clicked<-as.factor(data_test$clicked)

Naive_model<-naiveBayes(clicked~ .,data=data_train)
predict_train<-predict(Naive_model,data_train[-18])
predict_naive<-sapply(predict_train,conv_click)
actual_naive<-sapply(data_train$clicked,conv_click)
confusion.matrix(actual_naive,predict_naive)

predict_test<-predict(Naive_model,data_test[-18])
predict_NB<-sapply(predict_test,conv_click)
actual_NB<-sapply(data_test$clicked,conv_click)
confusion.matrix(actual_NB,predict_NB)

#decision tree
library("rpart")
library("rpart.plot")

treeModel <- rpart(clicked~site_showing_ad + platform.y + traffic_source + category_id + category_confidence_level + topic_id + topic_confidence_level + source_id,data = data_train,method = "class")
rpart.plot::rpart.plot(treeModel,fallen.leaves = FALSE,cex = 0.6)

predict_tree <- predict(treeModel,data_test[,-18],type = "class")
predict_tree1 <-sapply(predict_tree,conv_click)
actual_tree1 <- sapply(data_test$clicked,conv_click)
confusion.matrix(actual_tree1,predict_tree1)

# on train 
predict_tree_train <- predict(treeModel,data_train[,-18],type = "class")
predict_tree2 <-sapply(predict_tree_train,conv_click)
actual_tree2 <- sapply(data_train$clicked,conv_click)
confusion.matrix(actual_tree2,predict_tree2)

# ap function 
ap_function<-function(x){
  count_mone <- 0
  count_mehane <- 0
  ap<-0
  for(val in x){
    
    if(val==0){
      count_mehane<-count_mehane+1 
      } 
    else{
      count_mone<-count_mone+1 
      count_mehane<-count_mehane+1
      ap<-ap+(count_mone/count_mehane)
      }}
    result<-ap/count_mone
    return(result)}
average_precision <- ap_function(predict_NB)
average_precision

#map table building

map_table<- data.frame(display_id=display_id_test,ad_id=data_test$ad_id,clicked=predict_NB)

#map function

map_function<-function(x){
  mone<-0
  mehane<-0
  d_id<-0 #display_id
  result<-0
  i<-1
  if(x[i,3]==0){
    mehane<-mehane+1
  }
  else{
    mone<-mone+1
    mehane<-mehane+1
    result<-mone/mehane
  }
  for(val in x[2:nrow(x),1]){
    previous_row<-x[i,1]
    if(val==previous_row){
      if(x[i+1,3]==0){
        mehane<-mehane+1
      }
      else{
        mone<-mone+1
        mehane<-mehane+1
        result<-result+(mone/mehane)
      }
    }
    else{
      d_id<-d_id+1
      mone<-0
      mehane<-0
      if(x[i+1,3]==0){
        mehane<-mehane+1
      }
      else{
        mone<-mone+1
        mehane<-mehane+1
        result<-result+(mone/mehane)
      }
      
    }
    i<-i+1
  }
  map<-result/d_id
  return(map)
}

mean_average_precision <- map_function(map_table)
mean_average_precision
# logic regression (run of code done in a separate file, for computing reasons)
work_table <- read.csv(file = 'finish_table.csv',nrows = 50000)
split<-runif(nrow(work_table))>0.3

data_train<-work_table[split,]
data_test<-work_table[!split,]

#logistic regression
logModel<-glm(clicked~ad_id + site_showing_ad + uuid + timestamp.y + platform.y + geo_location.y + traffic_source + category_id + category_confidence_level + topic_id + topic_confidence_level + source_id + publisher_id + publish_time + destined_site_by_ad + campaign_id + advertiser_id,data=data_train,family="binomial")

#result prediction
predicted<- predict(logModel,data_test[,-19],type = "response")
predicted1<-ifelse(predicted>0.5,1,0)
conv01<-function(x){x<-ifelse(x=='1',1,0)}
actual1<-sapply(data_test$clicked,conv01)
library('SDMTools')
confusion.matrix(actual1,predicted1) 

